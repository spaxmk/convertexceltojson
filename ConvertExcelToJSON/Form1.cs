﻿using ExcelDataReader;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Z.Dapper.Plus;

namespace ConvertExcelToJSON
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public List<Worldcities> worldcities = new List<Worldcities>();

        Worldcities w1 = new Worldcities()
        {
            id = "1",
            city_ascii = "Sid",
            lat = "12345678",
            lng = "-12345657",
            country = "Serbia",
            iso2 = "22240",
            admin_name = "22240",
            capital = "15000",
            population = "7000"
        };

        Worldcities w2 = new Worldcities()
        {
            id = "2",
            city_ascii = "Sid",
            lat = "12345678",
            lng = "-12345657",
            country = "Serbia",
            iso2 = "22240",
            admin_name = "22240",
            capital = "15000",
            population = "7000"
        };


        private void cboSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = tableCollection[cboSheet.SelectedItem.ToString()];
            //dataGridView1.DataSource = dt;
            if (dt != null)
            {
                
                for(int i=0; i<dt.Rows.Count; i++)
                {
                    Worldcities worldCitie = new Worldcities();
                    worldCitie.id = dt.Rows[i]["id"].ToString();
                    worldCitie.city_ascii = dt.Rows[i]["city_ascii"].ToString();
                    worldCitie.lat = dt.Rows[i]["lat"].ToString();
                    worldCitie.lng = dt.Rows[i]["lng"].ToString();
                    worldCitie.country = dt.Rows[i]["country"].ToString();
                    worldCitie.iso2 = dt.Rows[i]["iso2"].ToString();
                    worldCitie.iso3 = dt.Rows[i]["iso3"].ToString();
                    worldCitie.admin_name = dt.Rows[i]["admin_name"].ToString();
                    worldCitie.capital = dt.Rows[i]["capital"].ToString();
                    worldCitie.population = dt.Rows[i]["population"].ToString();
                    worldcities.Add(worldCitie);
                }
                worldcitiesBindingSource.DataSource = worldcities;
            }
        }

        DataTableCollection tableCollection;

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "*.xls|*.xlsx" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    txtFileName.Text = openFileDialog.FileName;
                    using(var stream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                            });
                            tableCollection = result.Tables;
                            cboSheet.Items.Clear();
                            foreach(DataTable table in tableCollection)
                            {
                                cboSheet.Items.Add(table.TableName); //add sheet to combobox
                            }
                        }
                    }
                }
            }
        }

        
        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["WorldcitiesContext"].ToString();
                DapperPlusManager.Entity<Worldcities>().Table("worldcities");
                List<Worldcities> worldcities = worldcitiesBindingSource.DataSource as List<Worldcities>;
                if (worldcities != null)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.BulkInsert(worldcities);
                    }
                    MessageBox.Show("Finish");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            try
            {
                string strJSON = JsonConvert.SerializeObject(w1);
                strJSON += JsonConvert.SerializeObject(w2);
                MessageBox.Show(strJSON);
                File.WriteAllText(@"json-w1.json", strJSON);
                MessageBox.Show("Finish");

                //for (int i = 0; i < worldcities.Count; i++)
                //{
                //    string strJSON = JsonConvert.SerializeObject(worldcities[i].id + " " + worldcities[i].city_ascii + " " + worldcities[i].lat + " " + worldcities[i].lng);
                //    File.WriteAllText(@"json.json", strJSON);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
