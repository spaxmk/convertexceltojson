﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertExcelToJSON
{
    public class Worldcities
    {
        public string id { get; set; }
        public string city_ascii { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string country { get; set; }
        public string iso2 { get; set; }
        public string iso3 { get; set; }
        public string admin_name { get; set; }
        public string capital { get; set; }
        public string population { get; set; }

        public List<Worldcities> worldCitiesList = new List<Worldcities>();

        public Worldcities (string id, string city_ascii, string lat, string lng, string country, string iso2, string iso3, string admin_name, string capital, string population)
        {
            this.id = id;
            this.city_ascii = city_ascii;
            this.lat = lat;
            this.lng = lng;
            this.country = country;
            this.iso2 = iso2;
            this.admin_name = admin_name;
            this.capital = capital;
            this.population = population;
        }

        public Worldcities()
        {
            
        }

    }
}
